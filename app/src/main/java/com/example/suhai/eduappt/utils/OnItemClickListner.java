package com.example.suhai.eduappt.utils;

import android.view.View;

public interface OnItemClickListner {

    void onItemClickListener(View v,Object o);
}
