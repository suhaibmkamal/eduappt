package com.example.suhai.eduappt.model;

public interface IBooklet {

    /*"tutor": 6,
        "type": "BOOKLET",
        "subject": "what is this",
        "price": 12.32,
        "year_published": "2012-01-21",
        "education_system": "SAT",
        "stock": 12*/

    long getTutor();
    String getType();
    String getSubject();
    double getPrice();
    String getPublishedDate();
    String getEducationSystem();
    int getStock();

    void setTutor( long tutor);
    void setType(String type );
    void setSubject( String subject );
    void setPrice(double price);
    void setPublishedDate(String year_published);
    void setEducationSystem(String education_system);
    void setStock(int stock);
}
