package com.example.suhai.eduappt.view;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;

public class SplashActivity extends AppCompatActivity {

    SharedPrefrenceManager sharedPrefrenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        sharedPrefrenceManager = SharedPrefrenceManager.getInctence(this);

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                // This method will be executed once the timer is over
                if(sharedPrefrenceManager.getIsLoggedIn()){
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, 5000);
    }
}
