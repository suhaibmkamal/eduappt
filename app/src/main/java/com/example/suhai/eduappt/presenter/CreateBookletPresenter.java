package com.example.suhai.eduappt.presenter;

import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.retrofit.IMethodsToRequest;
import com.example.suhai.eduappt.retrofit.ServerManager;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;
import com.example.suhai.eduappt.view.BookletsView;
import com.example.suhai.eduappt.view.ISignupView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class CreateBookletPresenter implements ICreateBookletPresenter {

    IMethodsToRequest methodsToRequest;
    BookletsView bookletsView;
    CompositeDisposable compositeDisposable;
    SharedPrefrenceManager sharedPrefrenceManager;

    public CreateBookletPresenter(BookletsView bookletsView, SharedPrefrenceManager sharedPrefrenceManager) {
        this.bookletsView = bookletsView;
        this.sharedPrefrenceManager = sharedPrefrenceManager;
        methodsToRequest = ServerManager.getInstance();
        compositeDisposable = new CompositeDisposable();
        booklet = new Booklet();
        booklet.setType("BOOKLET");
    }

    Booklet booklet;
    @Override
    public void setType(String type) {

    }

    @Override
    public void setSubject(String subject) {
        booklet.setSubject(subject);
    }

    @Override
    public void setPrice(double price) {

        booklet.setPrice(price);
    }

    @Override
    public void setPublishedDate(String year_published) {

        booklet.setPublishedDate(year_published);
    }

    @Override
    public void setEducationSystem(String education_system) {

        booklet.setEducationSystem(education_system);
    }

    @Override
    public void setStock(int stock) {

        booklet.setStock(stock);
    }

    @Override
    public void onCreateBooklet() {

        compositeDisposable.add(methodsToRequest.createBooklets("JWT "+sharedPrefrenceManager.getToken(),booklet)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<Booklet>() {
                    @Override
                    public void onNext(Booklet booklet) {
                        bookletsView.onCreatBookletSuccessfuly(booklet);
                    }

                    @Override
                    public void onError(Throwable e) {

                        bookletsView.onErrorOnLogin(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    public void clearCompositeDisposable(){
        compositeDisposable.clear();
    }
}
