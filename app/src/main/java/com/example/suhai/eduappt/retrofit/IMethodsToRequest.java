package com.example.suhai.eduappt.retrofit;




import com.example.suhai.eduappt.model.Appointment;
import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.model.LoginObject;
import com.example.suhai.eduappt.model.User;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IMethodsToRequest {

    @Headers("Content-Type: application/json")
    @POST("sign-up")
    Observable<User> signUp (@Body User user);

    @Headers("Content-Type: application/json")
    @POST("/sign-in")
    Observable<User> signIn (@Body LoginObject loginObject);

    @Headers("Content-Type: application/json")
    @PATCH("/user")
    Observable<User> update (@Header("Authorization") String authorization,@Body User user  );

    @Headers("Content-Type: application/json")
    @GET("/teachers")
    Observable<ArrayList<User>> getTeachers (@Header("Authorization") String authorization, @Query("education") String edu);


    @Headers("Content-Type: application/json")
    @GET("/booklets")
    Observable<ArrayList<Booklet>> getBooklets (@Header("Authorization") String authorization);

    @Headers("Content-Type: application/json")
    @POST("/booklets")
    Observable<Booklet> createBooklets (@Header("Authorization") String authorization,@Body Booklet booklet);


    @Headers("Content-Type: application/json")
    @GET("/appointments")
    Observable<ArrayList<Appointment>> getAppointments (@Header("Authorization") String authorization);

    @Headers("Content-Type: application/json")
    @POST("/appointments")
    Observable<Appointment> createAppointment (@Header("Authorization") String authorization,@Body Appointment appointment);

}
