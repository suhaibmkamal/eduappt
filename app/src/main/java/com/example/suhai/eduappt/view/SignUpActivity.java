package com.example.suhai.eduappt.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.presenter.SignupPresenter;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;

public class SignUpActivity extends AppCompatActivity implements ISignupView, View.OnClickListener {


    EditText etUserName;
    EditText etPassword;
    EditText etEmail;
    EditText etBirthdate;
    EditText etPhone;
    EditText etAddress;

    EditText etConfirmPassword;
    EditText etMaxNumber;
    EditText etPrice;
    EditText etCardNumber;
    EditText etCardExpiryDate;
    RadioButton rbMale;
    RadioButton rbFeMale;
    RadioButton rbStudent;
    RadioButton rbTeacher;
    RadioButton rbTwjihi;
    RadioButton rbIG;
    RadioButton rbSAT;
    CheckBox isGroup;
    Button btnSignUp;

    RadioGroup rgEduType;

    SignupPresenter signupPresenter;
    private boolean isEdit;
    User user;
    SharedPrefrenceManager sharedPrefrenceManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        sharedPrefrenceManager = SharedPrefrenceManager.getInctence(this);


        signupPresenter = new SignupPresenter(this,sharedPrefrenceManager);

        if(getIntent().hasExtra("Edit")) {
            if (getIntent().getExtras().containsKey("Edit")) {
                isEdit = true;
                user = sharedPrefrenceManager.getUser();
            }
        }
        init();
    }

    public void init() {

        etUserName = findViewById(R.id.etUsername);
        etEmail = findViewById(R.id.etEmail);
        etBirthdate = findViewById(R.id.etBirthDate);
        etPhone = findViewById(R.id.etPhone);
        etAddress = findViewById(R.id.etAddress);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        etMaxNumber = findViewById(R.id.etMaxNumber);
        etCardNumber = findViewById(R.id.etCardNumber);
        etCardExpiryDate = findViewById(R.id.etCardExpiryDate);
        rgEduType = findViewById(R.id.rgEduType);
        etPrice = findViewById(R.id.etPrice);
        rbMale = findViewById(R.id.rbMale);
        rbFeMale = findViewById(R.id.rbFemale);
        rbStudent = findViewById(R.id.rbStudent);
        rbTeacher = findViewById(R.id.rbTeacher);
        rbTwjihi = findViewById(R.id.rbTwjihi);
        rbIG = findViewById(R.id.rbIGCSE);
        rbSAT = findViewById(R.id.rbSAT);
        isGroup = findViewById(R.id.cbIsGroup);
        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(this);

        if (isEdit) {
            btnSignUp.setText(R.string.edit);
            signupPresenter.setUser(user);
            etUserName.setText(user.getUsername());
            etPassword.setText(sharedPrefrenceManager.getLoginObj().getPassword());
            etConfirmPassword.setText(sharedPrefrenceManager.getLoginObj().getPassword());
            etBirthdate.setText(user.getDate_of_birth());
            etPhone.setText(String.valueOf(user.getPhone()));
            etAddress.setText(user.getAddress());
            etMaxNumber.setText(String.valueOf(user.getMax_number_per_group()));
            etPrice.setText(String.valueOf(user.getPrice_per_hour()));
            etCardNumber.setText(String.valueOf(user.getCard_number()));
            etCardExpiryDate.setText(String.valueOf(user.getCard_expire_date()));
            isGroup.setChecked(user.isGroup_lectures());
            if (user.getType().equals(User.UserType.STUDENT.name())) {
                rbStudent.setChecked(true);
            } else {

                rbTeacher.setChecked(true);
            }

            if (user.getEducation().equals(User.EducationType.TAWJIHI.name())) {
                rbTwjihi.setChecked(true);
            } else if (user.getEducation().equals(User.EducationType.IGCSE.name())) {

                rbIG.setChecked(true);
            }else{

                rbSAT.setChecked(true);
            }

            if (user.getGender().equals(User.UserGender.male.name())) {
                rbMale.setChecked(true);
            } else {

                rbFeMale.setChecked(true);
            }

        }

    }

    @Override
    public void onSignUpSuccessfully(User user, boolean isEdit) {
        if (isEdit) {
            Toast.makeText(this, "Updated Successfully", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(SignUpActivity.this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            sharedPrefrenceManager.saveUser(user);
            startActivity(i);

        } else {
            Toast.makeText(this, "Registered Successfully", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
    }

    @Override
    public void onErrorOnSignUp(Throwable e) {

        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        signupPresenter.clearCompositeDisposable();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnSignUp:
                initUserObj();
                if (isEdit) {
                    signupPresenter.updateUser();
                } else {
                    signupPresenter.onSignup();
                }

                break;

        }
    }

    private void initUserObj() {

        if(!etUserName.getText().toString().isEmpty())
        signupPresenter.setUserName(etUserName.getText().toString());
        if(!etEmail.getText().toString().isEmpty())
        signupPresenter.validateEmail(etEmail.getText().toString());
        if(!etBirthdate.getText().toString().isEmpty())
        signupPresenter.setBirthDate(etBirthdate.getText().toString());
        if(!etAddress.getText().toString().isEmpty())
        signupPresenter.setAddress(etAddress.getText().toString());
        if(!etCardExpiryDate.getText().toString().isEmpty())
        signupPresenter.setCardExpireDate(etCardExpiryDate.getText().toString());
        if(!etCardNumber.getText().toString().isEmpty())
        signupPresenter.setCardNumber(etCardNumber.getText().toString());
        if(!etAddress.getText().toString().isEmpty())
        signupPresenter.setAddress(etAddress.getText().toString());
        if(!etMaxNumber.getText().toString().isEmpty())
        signupPresenter.setMaxnumberInGroup(Integer.valueOf(etMaxNumber.getText().toString()));
        if(!etPrice.getText().toString().isEmpty())
        signupPresenter.setPrice(Integer.valueOf(etPrice.getText().toString()));
        if(!etPassword.getText().toString().isEmpty()&&!etConfirmPassword.getText().toString().isEmpty())
        signupPresenter.validatePassword(etPassword.getText().toString(),
                etConfirmPassword.getText().toString());
        if(!etPhone.getText().toString().isEmpty())
        signupPresenter.setPhone(Long.valueOf(etPhone.getText().toString()));
        signupPresenter.setUserGender(rbMale.isChecked());
        signupPresenter.setUserType(rbStudent.isChecked());
        switch (rgEduType.getCheckedRadioButtonId()){
            case R.id.rbSAT:
                signupPresenter.setUserEduType(User.EducationType.SAT.name());
                break;

            case R.id.rbIGCSE:
                signupPresenter.setUserEduType(User.EducationType.IGCSE.name());
                break;

            case R.id.rbTwjihi:

                signupPresenter.setUserEduType(User.EducationType.TAWJIHI.name());
                break;
        }
        signupPresenter.setIsGroupAllowed(isGroup.isChecked());
    }
}
