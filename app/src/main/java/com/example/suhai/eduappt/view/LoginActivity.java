package com.example.suhai.eduappt.view;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.presenter.LoginPresenter;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,ILoginView {

    Button btnSignUp;
    Button btnLogin;
    EditText etUserName;
    EditText etPassword;
    TextView tvAboutUs;
    ImageView ivLynda;
    ImageView ivUdemy;
    ImageView ivEdx;

    LoginPresenter loginPresenter;

    SharedPrefrenceManager sharedPrefrenceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnSignUp = findViewById(R.id.btnSignUp);
        etUserName = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvAboutUs = findViewById(R.id.tvAboutUs);

        ivLynda = findViewById(R.id.lyndaIv);
        ivUdemy = findViewById(R.id.udemyIv);
        ivEdx = findViewById(R.id.edxIv);
        btnLogin.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        tvAboutUs.setOnClickListener(this);
        ivLynda.setOnClickListener(this);
        ivUdemy.setOnClickListener(this);
        ivEdx.setOnClickListener(this);

        loginPresenter = new LoginPresenter(this);

        sharedPrefrenceManager = SharedPrefrenceManager.getInctence(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btnLogin:
                initLoginObj();
                loginPresenter.onLogin();
                break;

            case R.id.btnSignUp:
                Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(i);
                break;
            case R.id.tvAboutUs:
                Intent i2 = new Intent(LoginActivity.this, AboutUsActivity.class);
                startActivity(i2);
                break;
            case R.id.lyndaIv:
                openOnBrowser("https://www.lynda.com/");
                break;
            case R.id.udemyIv:
                openOnBrowser("https://www.udemy.com/");
                break;
            case R.id.edxIv:
                openOnBrowser("https://www.edx.org/");

                break;
        }

    }

    private void initLoginObj() {

        loginPresenter.setUserName(etUserName.getText().toString());
        loginPresenter.setPassword(etPassword.getText().toString());
    }

    void openOnBrowser(String website){
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse(website)));
    }

    @Override
    public void onLoginSuccessfully(User user) {

        Toast.makeText(this,"hello "+user.getUsername(),Toast.LENGTH_SHORT).show();
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        i.putExtra("user",user);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        sharedPrefrenceManager.saveUser(user);
        sharedPrefrenceManager.saveToken(user.getToken());
        sharedPrefrenceManager.saveLoginObj(loginPresenter.getLoginObj());
        sharedPrefrenceManager.setLoggedin();
        startActivity(i);
    }

    @Override
    public void onErrorOnLogin(Throwable e) {

        Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.clearCompositeDisposable();
    }
}
