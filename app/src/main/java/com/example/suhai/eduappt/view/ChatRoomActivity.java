package com.example.suhai.eduappt.view;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.User;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ChatRoomActivity extends AppCompatActivity implements View.OnClickListener ,ChildEventListener {

    ImageView btnSend;
    EditText etMessage;
    ListView lvChat;
    DatabaseReference mDatabaseRef;

    ArrayAdapter<String> adapter;
    User user;
    String chatName;
    ArrayList<String> userRoomsNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        setTitle(R.string.chat);
        user = (User)getIntent().getExtras().get("user");
        chatName =getIntent().getExtras().get("roomName").toString();
        etMessage =(EditText) findViewById(R.id.etMessage);
        btnSend= (ImageView) findViewById(R.id.btnSend);
        lvChat = (ListView) findViewById(R.id.lvChat);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        userRoomsNames = new ArrayList<>();
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,userRoomsNames);
        lvChat.setAdapter(adapter);
        btnSend.setOnClickListener(this);
        mDatabaseRef.child("Messages").child(chatName).addChildEventListener(this);
    }


    private void append_chat_conversation(DataSnapshot dataSnapshot) {

        Iterator i = dataSnapshot.getChildren().iterator();
        Set<String> set = new HashSet<String>();

        while (i.hasNext()){
            String text =(String) ((DataSnapshot)i.next()).getValue();
            String name =(String) ((DataSnapshot) i.next()).getValue();

            String message =name  +":"+text;
            set.add(message);

        }

        adapter.addAll(set);

        adapter.notifyDataSetChanged();
        lvChat.smoothScrollToPosition(adapter.getCount());

    }

    @Override
    public void onClick(View view)
    {


        Map<String,Object> messageMap = new HashMap<String, Object>();
        messageMap.put("Message",etMessage.getText().toString());
        messageMap.put("Sender",user.getUsername());
        mDatabaseRef.child("Messages").child(chatName).push().updateChildren(messageMap);
        etMessage.setText("");

    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        append_chat_conversation(dataSnapshot);
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        append_chat_conversation(dataSnapshot);
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
