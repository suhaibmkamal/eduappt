package com.example.suhai.eduappt.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.model.User;

public class BookletViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView tvSubject;
    TextView tvStock;
    TextView tvPublishDate;
    TextView tvEduSystem;
    TextView tvPrice;
    Button btnBuyBooklet;

    OnItemClickListner onItemClickListner;
    Booklet booklet;

    public BookletViewHolder(@NonNull View itemView, OnItemClickListner onItemClickListner) {
        super(itemView);

        tvSubject = itemView.findViewById(R.id.tvSubject);
        tvStock = itemView.findViewById(R.id.stockTv);
        tvPublishDate = itemView.findViewById(R.id.publishDateTv);
        tvEduSystem = itemView.findViewById(R.id.eduSystemTv);
        tvPrice = itemView.findViewById(R.id.priceTv);
        btnBuyBooklet = itemView.findViewById(R.id.btnBuyBooklet);

        this.onItemClickListner = onItemClickListner;

        btnBuyBooklet.setOnClickListener(this);

    }

    public void setData(Booklet booklet) {

        this.booklet = booklet;
        tvStock.setText(booklet.getStock() + " In Stock");
        tvSubject.setText(booklet.getSubject());
        tvPrice.setText(booklet.getPrice() + "JOD");
        tvPublishDate.setText(booklet.getPublishedDate());
        tvEduSystem.setText(booklet.getEducationSystem());

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnBuyBooklet:
                onItemClickListner.onItemClickListener(btnBuyBooklet, booklet);
                break;
        }

    }
}
