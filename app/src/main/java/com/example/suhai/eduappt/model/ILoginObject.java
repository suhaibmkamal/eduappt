package com.example.suhai.eduappt.model;

public interface ILoginObject {

    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);
}
