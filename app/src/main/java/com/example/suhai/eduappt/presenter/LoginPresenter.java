package com.example.suhai.eduappt.presenter;

import com.example.suhai.eduappt.model.LoginObject;
import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.retrofit.IMethodsToRequest;
import com.example.suhai.eduappt.retrofit.ServerManager;
import com.example.suhai.eduappt.view.ILoginView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenter implements ILoginPresenter {
    IMethodsToRequest methodsToRequest;
    ILoginView loginView;
    CompositeDisposable compositeDisposable;

    LoginObject loginObject;

    public LoginPresenter(ILoginView loginView){
        this.loginView = loginView;
        methodsToRequest = ServerManager.getInstance();
        compositeDisposable = new CompositeDisposable();

        loginObject = new LoginObject();
    }
    @Override
    public void onLogin() {

        compositeDisposable.add(methodsToRequest.signIn(loginObject)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<User>() {
                    @Override
                    public void onNext(User user) {

                        loginView.onLoginSuccessfully(user);
                    }

                    @Override
                    public void onError(Throwable e) {
                        loginView.onErrorOnLogin(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));

    }

    private void saveUser(User user){


    }

    @Override
    public void setUserName(String userName) {

        loginObject.setUsername(userName);
    }

    @Override
    public void setPassword(String password) {
        loginObject.setPassword(password);
    }

    @Override
    public LoginObject getLoginObj() {
        return loginObject;
    }

    public void clearCompositeDisposable(){
        compositeDisposable.clear();
    }
}
