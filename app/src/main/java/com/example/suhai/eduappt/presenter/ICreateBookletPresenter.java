package com.example.suhai.eduappt.presenter;

public interface ICreateBookletPresenter {

    void setType(String type );
    void setSubject( String subject );
    void setPrice(double price);
    void setPublishedDate(String year_published);
    void setEducationSystem(String education_system);
    void setStock(int stock);

    void onCreateBooklet();
}
