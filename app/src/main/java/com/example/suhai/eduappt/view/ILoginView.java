package com.example.suhai.eduappt.view;

import com.example.suhai.eduappt.model.User;

public interface ILoginView {

    void onLoginSuccessfully(User user);
    void onErrorOnLogin(Throwable e);
}
