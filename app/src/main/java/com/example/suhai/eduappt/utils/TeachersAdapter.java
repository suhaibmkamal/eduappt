package com.example.suhai.eduappt.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.User;

import java.util.ArrayList;

public class TeachersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    OnItemClickListner onItemClickListner;

    public TeachersAdapter(ArrayList<User> users,OnItemClickListner onItemClickListner) {
        this.users = users;
        this.onItemClickListner = onItemClickListner;
    }

    ArrayList<User> users;
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new TeacherViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_holder_teacher, viewGroup, false),onItemClickListner);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        TeacherViewHolder teacherViewHolder = (TeacherViewHolder)viewHolder;
        teacherViewHolder.setData(users.get(i));

    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}
