package com.example.suhai.eduappt.presenter;

import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.retrofit.IMethodsToRequest;
import com.example.suhai.eduappt.retrofit.ServerManager;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;
import com.example.suhai.eduappt.view.BookletsView;
import com.example.suhai.eduappt.view.TeachersView;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class BookletsPresenter implements IBookletsPresenter{

    IMethodsToRequest methodsToRequest;
    BookletsView bookletsView;
    CompositeDisposable compositeDisposable;

    SharedPrefrenceManager sharedPrefrenceManager;

    public BookletsPresenter(BookletsView bookletsView, SharedPrefrenceManager sharedPrefrenceManager) {
        this.bookletsView = bookletsView;
        this.sharedPrefrenceManager = sharedPrefrenceManager;
        methodsToRequest = ServerManager.getInstance();
        compositeDisposable = new CompositeDisposable();
        userToken = sharedPrefrenceManager.getToken();
    }

    String userToken;
    @Override
    public void getBooklets() {

        compositeDisposable.add(methodsToRequest.getBooklets("JWT " + userToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<ArrayList<Booklet>>() {
                    @Override
                    public void onNext(ArrayList<Booklet> booklets) {

                        bookletsView.onGetSuccessfully(booklets);
                    }

                    @Override
                    public void onError(Throwable e) {
                        bookletsView.onErrorOnLogin(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));

    }

    public void clearCompositeDisposable(){
        compositeDisposable.clear();
    }
}
