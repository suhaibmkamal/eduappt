package com.example.suhai.eduappt.model;

import android.os.Parcelable;

import java.io.Serializable;

public class User implements IUser,Serializable {

    /*"username": "teacher3",
	"password":"student1234",
	"phone":3211,
	"type" : "TEACHER",
	"gender":"male",
	"education":"IGCSE",
	"date_of_birth":"2012-12-01",
	"address":"some crzay string",
	"card_number":"123453123122",
	"card_expire_date":"2012-01-01",
	"group_lectures":false,
	"max_number_per_group":0,
	"price_per_hour":12*/

    long id;
    String username;
    String password;
    String date_of_birth;
    String address;//
    String card_number;
    String card_expire_date;
    boolean group_lectures;//
    long phone;
    String type;//
    String gender;
    int max_number_per_group;//
    int price_per_hour;//
    String education;//
    String token;//

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {

        this.password = password;
    }

    @Override
    public String getDate_of_birth() {
        return date_of_birth;
    }

    @Override
    public void setDate_of_birth(String date_of_birth) {

        this.date_of_birth = date_of_birth;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {

        this.address = address;
    }

    @Override
    public String getCard_number() {
        return card_number;
    }

    @Override
    public void setCard_number(String card_number) {

        this.card_number =card_number;
    }

    @Override
    public String getCard_expire_date() {
        return card_expire_date;
    }

    @Override
    public void setCard_expire_date(String card_expire_date) {
        this.card_expire_date = card_expire_date;
    }

    @Override
    public boolean isGroup_lectures() {
        return group_lectures;
    }

    @Override
    public void setGroup_lectures(boolean group_lectures) {
        this.group_lectures = group_lectures;
    }

    @Override
    public long getPhone() {
        return phone;
    }

    @Override
    public void setPhone(long phone) {

        this.phone = phone;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public void setGender(String gender) {

        this.gender = gender;
    }

    @Override
    public int getMax_number_per_group() {
        return max_number_per_group;
    }

    @Override
    public void setMax_number_per_group(int max_number_per_group) {

        this.max_number_per_group = max_number_per_group;
    }

    @Override
    public int getPrice_per_hour() {
        return price_per_hour;
    }

    @Override
    public void setPrice_per_hour(int price_per_hour) {

        this.price_per_hour = price_per_hour;
    }

    @Override
    public String getEducation() {
        return education;
    }

    @Override
    public void setEducation(String education) {
        this.education = education;

    }

    @Override
    public void setId(long id) {

        this.id = id;
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public void setToken(String token) {

        this.token = token;
    }


    public enum UserType implements Serializable {

        TEACHER("TEACHER"),STUDENT("STUDENT");

        UserType(String s) {

        }
    }

    public enum UserGender implements Serializable{

        female("female"),male("male");

        UserGender(String s) {

        }
    }

    public enum EducationType implements Serializable{

        IGCSE("IGCSE"),TAWJIHI("TAWJIHI"),SAT("SAT") ;

        EducationType(String s) {

        }
    }


}
