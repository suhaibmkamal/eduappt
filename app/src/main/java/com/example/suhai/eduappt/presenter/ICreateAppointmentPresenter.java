package com.example.suhai.eduappt.presenter;

import java.util.Date;

public interface ICreateAppointmentPresenter {

    void setTutor( long tutor);
    void setStatus(String status );
    void setSubject( String subject );
    void setPrice(double price);
    void setTime(String time);
    void setDuration(String duration);
    void setAddress(String address);
    void onCreateAppointment();
    void getTeachers();
}
