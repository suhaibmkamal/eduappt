package com.example.suhai.eduappt.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.suhai.eduappt.model.LoginObject;
import com.example.suhai.eduappt.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SharedPrefrenceManager {

    public static final String EDUAPPT_SHARED = "eduapptSharedPrefs";
    public static final String USER = "User";
    public static final String LOGGEDIN = "loggedIn";
    private static final String LOGGEDIN_OBJ ="loggedInObj" ;
    private static final String TOKEN ="token" ;

    SharedPreferences sharedPreferences;
    static SharedPrefrenceManager sharedPrefrenceManager;

    public static synchronized SharedPrefrenceManager getInctence(Context context) {

        if (sharedPrefrenceManager == null) {
            sharedPrefrenceManager = new SharedPrefrenceManager(context);

        }
        return sharedPrefrenceManager;
    }

    SharedPrefrenceManager(Context context) {

        sharedPreferences = context.getSharedPreferences(EDUAPPT_SHARED,
                Context.MODE_PRIVATE);
    }

    public void saveString(String key, String value) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void setLoggedin() {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LOGGEDIN, true);
        editor.apply();
    }

    public void setLoggedOut() {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LOGGEDIN, false);
        editor.apply();
    }

    public void saveInt(String key, int value) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public void saveLong(String key, long value) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }
    public void saveLoginObj(LoginObject loginObject) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(LOGGEDIN_OBJ, new Gson().toJson(loginObject));
        editor.apply();
    }

    public LoginObject getLoginObj() {


        return new Gson().fromJson(sharedPreferences.getString(LOGGEDIN_OBJ, null), LoginObject.class);

    }

    public void saveUser(User user) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER, new Gson().toJson(user));
        editor.apply();
    }

    public void saveToken(String token) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOKEN, token);
        editor.apply();
    }

    public String  getToken(){
        return sharedPreferences.getString(TOKEN, "");
    }

    public User getUser() {


        return new Gson().fromJson(sharedPreferences.getString(USER, null), User.class);

    }

    public boolean getIsLoggedIn(){
        return sharedPreferences.getBoolean(LOGGEDIN, false);
    }
}
