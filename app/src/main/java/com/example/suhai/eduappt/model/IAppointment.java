package com.example.suhai.eduappt.model;

import java.util.Date;

public interface IAppointment {

    /*"subject": "English",
        "time": "2019-01-08T10:00:00Z",
        "address": "amman,BV",
        "price": 10,
        "tutor": 3,
        "duration": "03:00:00",
        "student": 6,
        "status": "WAITING"*/

    long getTutor();
    String getStatus();
    String getSubject();
    double getPrice();
    String getTime();
    String getDuration();
    String getAddress();


    void setTutor( long tutor);
    void setStatus(String status );
    void setSubject( String subject );
    void setPrice(double price);
    void setTime(String time);
    void setDuration(String duration);
    void setAddress(String address);

}
