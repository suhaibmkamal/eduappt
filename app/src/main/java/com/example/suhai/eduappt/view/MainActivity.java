package com.example.suhai.eduappt.view;

import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.utils.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView mainBNV;
    ActionBar mainActionBar;

    ViewPager mainVP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActionBar = getSupportActionBar();
        mainActionBar.setTitle(R.string.teachers);
        mainBNV = findViewById(R.id.mainBNV);
        mainVP = findViewById(R.id.mainVP);
        setupViewPager(mainVP);
        mainBNV.setOnNavigationItemSelectedListener(this);
    }

    private void setupViewPager(ViewPager viewPager) {


        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TeachersFragment());
        adapter.addFragment(new AppointmentsFragment());
        adapter.addFragment(new BookletsFragment());
        adapter.addFragment(new ChatsFragment());
        adapter.addFragment(new SettingsFragment());
        viewPager.setAdapter(adapter);
        mainVP.addOnPageChangeListener(this);


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.navigation_teachers:
                mainActionBar.setTitle(R.string.teacher);
                mainVP.setCurrentItem(0);
                return true;
            case R.id.navigation_Appointments:
                mainActionBar.setTitle(R.string.appointments);
                mainVP.setCurrentItem(1);
                return true;
            case R.id.navigation_Booklets:
                mainActionBar.setTitle(R.string.booklets);
                mainVP.setCurrentItem(2);
                return true;
            case R.id.navigation_chat:
                mainActionBar.setTitle(R.string.chat);
                mainVP.setCurrentItem(3);
                return true;
            case R.id.navigation_Settings:
                mainActionBar.setTitle(R.string.settings);
                mainVP.setCurrentItem(4);
                return true;
        }
        return false;
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

        mainBNV.getMenu().getItem(i).setChecked(true);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
