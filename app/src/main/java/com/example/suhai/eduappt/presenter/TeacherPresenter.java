package com.example.suhai.eduappt.presenter;

import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.retrofit.IMethodsToRequest;
import com.example.suhai.eduappt.retrofit.ServerManager;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;
import com.example.suhai.eduappt.view.ILoginView;
import com.example.suhai.eduappt.view.TeachersView;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class TeacherPresenter implements ITeacherPresenter{

    IMethodsToRequest methodsToRequest;
    TeachersView teachersView;
    CompositeDisposable compositeDisposable;

    SharedPrefrenceManager sharedPrefrenceManager;

    public TeacherPresenter(TeachersView teachersView, SharedPrefrenceManager sharedPrefrenceManager) {
        this.teachersView = teachersView;
        this.sharedPrefrenceManager = sharedPrefrenceManager;
        methodsToRequest = ServerManager.getInstance();
        compositeDisposable = new CompositeDisposable();
        userToken = sharedPrefrenceManager.getToken();
        eduType = sharedPrefrenceManager.getUser().getEducation();

    }

    String userToken;
    String eduType;
    @Override
    public void getTeachers() {

        compositeDisposable.add(methodsToRequest.getTeachers("JWT " + userToken,eduType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<ArrayList<User>>() {
                    @Override
                    public void onNext(ArrayList<User> users) {

                        teachersView.onGetSuccessfully(users);
                    }

                    @Override
                    public void onError(Throwable e) {
                        teachersView.onErrorOnLogin(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));

    }

    public void clearCompositeDisposable(){
        compositeDisposable.clear();
    }
}
