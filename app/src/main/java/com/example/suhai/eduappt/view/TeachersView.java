package com.example.suhai.eduappt.view;

import com.example.suhai.eduappt.model.User;

import java.util.ArrayList;

public interface TeachersView {

    void onGetSuccessfully(ArrayList<User> users);
    void onErrorOnLogin(Throwable e);
}
