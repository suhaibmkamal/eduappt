package com.example.suhai.eduappt.view;

import com.example.suhai.eduappt.model.User;

public interface ISignupView {

    void onSignUpSuccessfully(User user,boolean isEdit);
    void onErrorOnSignUp(Throwable e);
}
