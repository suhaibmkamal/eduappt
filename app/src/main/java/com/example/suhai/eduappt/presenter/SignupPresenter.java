package com.example.suhai.eduappt.presenter;

import android.content.Context;

import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.retrofit.IMethodsToRequest;
import com.example.suhai.eduappt.retrofit.ServerManager;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;
import com.example.suhai.eduappt.view.ISignupView;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class SignupPresenter implements ISignupPresenter {

    IMethodsToRequest methodsToRequest;
    ISignupView signupView;
    CompositeDisposable compositeDisposable;
    SharedPrefrenceManager sharedPrefrenceManager;

    User user;

    public SignupPresenter(ISignupView signupView, SharedPrefrenceManager sharedPrefrenceManager) {
        this.signupView = signupView;
        methodsToRequest = ServerManager.getInstance();
        compositeDisposable = new CompositeDisposable();
        this.sharedPrefrenceManager = sharedPrefrenceManager;
        user = new User();
    }

    @Override
    public void onSignup() {

        compositeDisposable.add(methodsToRequest.signUp(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<User>() {
                    @Override
                    public void onNext(User user) {
                        signupView.onSignUpSuccessfully(user, false);
                    }

                    @Override
                    public void onError(Throwable e) {

                        signupView.onErrorOnSignUp(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));

    }

    @Override
    public void updateUser() {
        compositeDisposable.add(methodsToRequest.update("JWT " + sharedPrefrenceManager.getToken(), user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<User>() {
                    @Override
                    public void onNext(User user) {
                        signupView.onSignUpSuccessfully(user, true);
                    }

                    @Override
                    public void onError(Throwable e) {

                        signupView.onErrorOnSignUp(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    @Override
    public void setUserName(String userName) {
        user.setUsername(userName);
    }

    @Override
    public void setPhone(long phone) {

        user.setPhone(phone);
    }

    @Override
    public void setMaxnumberInGroup(int maxNumber) {

        user.setMax_number_per_group(maxNumber);
    }

    @Override
    public void setIsGroupAllowed(boolean isGroupAllowed) {
        user.setGroup_lectures(isGroupAllowed);
    }

    @Override
    public void setPrice(int price) {

        user.setPrice_per_hour(price);

    }

    @Override
    public void validateEmail(String email) {

    }

    @Override
    public void setAddress(String address) {

        user.setAddress(address);
    }

    @Override
    public void setBirthDate(String birthDate) {
        user.setDate_of_birth(birthDate);

    }

    @Override
    public void setCardExpireDate(String cardExpireDate) {

        user.setCard_expire_date(cardExpireDate);
    }

    @Override
    public void setCardNumber(String cardNumber) {

        user.setCard_number(cardNumber);

    }

    @Override
    public void setUserType(boolean isStudent) {

        if (isStudent) {
            user.setType(User.UserType.STUDENT.name());
        } else {
            user.setType(User.UserType.TEACHER.name());
        }

    }

    @Override
    public void setUserGender(boolean isMale) {

        if (isMale) {

            user.setGender(User.UserGender.male.name());
        } else {

            user.setGender(User.UserGender.female.name());
        }
    }

    @Override
    public void setUserEduType(String isTawjihi) {


        user.setEducation(isTawjihi);


    }

    @Override
    public void validatePassword(String password, String rePassword) {

        if (rePassword.equals(password)) {
            user.setPassword(password);
        } else {

            signupView.onErrorOnSignUp(new Throwable("Please Check Your Password"));
        }

    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {

        this.user = user;
    }


    public void clearCompositeDisposable() {
        compositeDisposable.clear();
    }
}
