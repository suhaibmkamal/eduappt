package com.example.suhai.eduappt.model;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Appointment implements IAppointment {

    @SerializedName("subject")
    String subject;
    @SerializedName("time")
    String time;
    @SerializedName("address")
    String address;
    @SerializedName("price")
    double price;
    @SerializedName("tutor")
    long tutor;
    @SerializedName("duration")
    String duration;
    @SerializedName("status")
    String status;

    @Override
    public long getTutor() {
        return tutor;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String getTime() {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'");
        Date date = null;
        try {
            date = fmt.parse(time);
        } catch (ParseException e) {
            return time;
        }

        SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        return fmtOut.format(date);
    }

    @Override
    public String getDuration() {
        return duration;
    }

    @Override
    public String getAddress() {
        return address;
    }



    @Override
    public void setTutor(long tutor) {

        this.tutor = tutor;
    }

    @Override
    public void setStatus(String status) {

        this.status = status;
    }

    @Override
    public void setSubject(String subject) {

        this.subject = subject;
    }

    @Override
    public void setPrice(double price) {

        this.price = price;
    }

    @Override
    public void setTime(String time) {

        this.time = time;
    }

    @Override
    public void setDuration(String duration) {

        this.duration = duration;
    }

    @Override
    public void setAddress(String address) {

        this.address = address;
    }


}
