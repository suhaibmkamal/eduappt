package com.example.suhai.eduappt.presenter;

import com.example.suhai.eduappt.model.LoginObject;

public interface ILoginPresenter {

    void onLogin();

    void setUserName(String userName);
    void setPassword(String password);

    LoginObject getLoginObj();

}
