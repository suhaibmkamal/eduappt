package com.example.suhai.eduappt.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.model.User;

import java.util.ArrayList;

public class BookletsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    OnItemClickListner onItemClickListner;

    public BookletsAdapter(ArrayList<Booklet> booklets, OnItemClickListner onItemClickListner) {
        this.booklets = booklets;
        this.onItemClickListner = onItemClickListner;
    }

    ArrayList<Booklet> booklets;
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new BookletViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_holder_booklet, viewGroup, false),onItemClickListner);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        BookletViewHolder teacherViewHolder = (BookletViewHolder)viewHolder;
        teacherViewHolder.setData(booklets.get(i));

    }

    @Override
    public int getItemCount() {
        return booklets.size();
    }
}
