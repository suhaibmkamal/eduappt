package com.example.suhai.eduappt.retrofit;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServerManager {

    private static  IMethodsToRequest ourInstance;

    public static String BASE_URL ="http://192.168.1.10:8000/";

    public static synchronized IMethodsToRequest getInstance() {
        if (ourInstance == null){

            ourInstance = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build().create(IMethodsToRequest.class);
            return ourInstance;
        }else {
            return ourInstance;
        }
    }

    private ServerManager() {
    }
}
