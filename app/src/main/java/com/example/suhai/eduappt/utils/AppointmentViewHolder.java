package com.example.suhai.eduappt.utils;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.Appointment;
import com.example.suhai.eduappt.model.Booklet;

public class AppointmentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView tvSubject;
    TextView tvPrice;
    TextView tvAddress;
    TextView tvTime;
    TextView tvDuration;
    TextView tvStatus;
    TextView tvRate;
    TextView tvAssignment;

    Context context;





    public AppointmentViewHolder(@NonNull View itemView) {
        super(itemView);

        tvSubject = itemView.findViewById(R.id.tvSubject);
        tvAddress = itemView.findViewById(R.id.addressTv);
        tvDuration = itemView.findViewById(R.id.durationTv);
        tvTime = itemView.findViewById(R.id.timeTv);
        tvPrice = itemView.findViewById(R.id.priceTv);
        tvStatus = itemView.findViewById(R.id.statusTv);
        tvRate = itemView.findViewById(R.id.rateTv);
        tvAssignment = itemView.findViewById(R.id.assignmentTv);

        tvRate.setOnClickListener(this);
        tvAssignment.setOnClickListener(this);

        context = itemView.getContext();




    }

    public void setData(Appointment appointment) {

        tvTime.setText(appointment.getTime());
        tvSubject.setText(appointment.getSubject());
        tvPrice.setText(appointment.getPrice() + "JOD");
        tvDuration.setText(appointment.getDuration());
        tvStatus.setText(appointment.getStatus());
        tvAddress.setText(appointment.getAddress());

        if(appointment.getStatus().equals("FINISHED")){

            tvRate.setVisibility(View.VISIBLE);
            tvAssignment.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.rateTv:
                show();
                break;

            case R.id.assignmentTv:
                break;
        }
    }

    public void show()
    {

        final Dialog d = new Dialog(context);
        d.setTitle("NumberPicker");
        d.setContentView(R.layout.dialog_rate);
        Button b1 = (Button) d.findViewById(R.id.btnCancel);
        Button b2 = (Button) d.findViewById(R.id.btnSet);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.npRate);
        np.setMaxValue(5); // max value 100
        np.setMinValue(0);   // min value 0
        np.setWrapSelectorWheel(false);
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss(); // dismiss the dialog
            }
        });
        d.show();


    }
}
