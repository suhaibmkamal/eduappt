package com.example.suhai.eduappt.view;

import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.model.User;

import java.util.ArrayList;

public interface BookletsView {

    void onGetSuccessfully(ArrayList<Booklet> booklets);
    void onCreatBookletSuccessfuly(Booklet booklet);
    void onErrorOnLogin(Throwable e);
}
