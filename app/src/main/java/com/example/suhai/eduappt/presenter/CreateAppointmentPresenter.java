package com.example.suhai.eduappt.presenter;

import com.example.suhai.eduappt.model.Appointment;
import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.retrofit.IMethodsToRequest;
import com.example.suhai.eduappt.retrofit.ServerManager;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;
import com.example.suhai.eduappt.view.BookletsView;
import com.example.suhai.eduappt.view.IAppointmentView;

import java.util.ArrayList;
import java.util.Date;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class CreateAppointmentPresenter implements ICreateAppointmentPresenter {

    IMethodsToRequest methodsToRequest;
    IAppointmentView appointmentView;
    CompositeDisposable compositeDisposable;
    SharedPrefrenceManager sharedPrefrenceManager;

    Appointment appointment;

    public CreateAppointmentPresenter(IAppointmentView appointmentView, SharedPrefrenceManager sharedPrefrenceManager) {
        this.appointmentView = appointmentView;
        this.sharedPrefrenceManager = sharedPrefrenceManager;
        methodsToRequest = ServerManager.getInstance();
        compositeDisposable = new CompositeDisposable();
        appointment = new Appointment();
    }

    @Override
    public void setTutor(long tutor) {
        appointment.setTutor(tutor);
    }

    @Override
    public void setStatus(String status) {

    }

    @Override
    public void setSubject(String subject) {

        appointment.setSubject(subject);
    }

    @Override
    public void setPrice(double price) {
        appointment.setPrice(price);
    }

    @Override
    public void setTime(String time) {

        appointment.setTime(time);
    }

    @Override
    public void setDuration(String duration) {

        appointment.setDuration(duration);
    }

    @Override
    public void setAddress(String address) {

        appointment.setAddress(address);
    }

    @Override
    public void onCreateAppointment() {

        compositeDisposable.add(methodsToRequest.createAppointment("JWT " + sharedPrefrenceManager.getToken(), appointment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<Appointment>() {
                    @Override
                    public void onNext(Appointment booklet) {
                        appointmentView.onCreatAppointmentSuccessfuly(booklet);
                    }

                    @Override
                    public void onError(Throwable e) {

                        appointmentView.onErrorOnLogin(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));

    }

    @Override
    public void getTeachers() {

        compositeDisposable.add(methodsToRequest
                .getTeachers("JWT " + sharedPrefrenceManager.getToken(),
                        sharedPrefrenceManager.getUser().getEducation())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<ArrayList<User>>() {
                    @Override
                    public void onNext(ArrayList<User> users) {

                        appointmentView.onGetTeahcersSuccessfully(users);
                    }

                    @Override
                    public void onError(Throwable e) {
                        appointmentView.onErrorOnLogin(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));

    }

    public void clearCompositeDisposable() {
        compositeDisposable.clear();
    }
}
