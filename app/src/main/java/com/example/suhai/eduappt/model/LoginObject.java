package com.example.suhai.eduappt.model;

import com.example.suhai.eduappt.view.ILoginView;

public class LoginObject implements ILoginObject{

    String username;
    String password;
    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {

        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {

        this.password = password;
    }
}
