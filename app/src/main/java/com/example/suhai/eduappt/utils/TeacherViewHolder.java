package com.example.suhai.eduappt.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.User;

public class TeacherViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView tvTeacherName;
    TextView tvPrice;
    ImageView ivChatTeacher;
    ImageView ivBookAppointment;
    ImageView firstStar;
    ImageView secondStar;
    ImageView thirdStar;
    ImageView fourthStar;
    ImageView fifthStar;
    Context context;


    OnItemClickListner onItemClickListner;
    User teacher ;

    public TeacherViewHolder(@NonNull View itemView,OnItemClickListner onItemClickListner) {
        super(itemView);

        tvTeacherName = itemView.findViewById(R.id.teacherNameTv);
        tvPrice = itemView.findViewById(R.id.priceTv);
        ivChatTeacher = itemView.findViewById(R.id.chatTeacherIv);
        ivBookAppointment = itemView.findViewById(R.id.bookTeacherIv);
        firstStar = itemView.findViewById(R.id.firstStar);
        secondStar = itemView.findViewById(R.id.secondStar);
        thirdStar = itemView.findViewById(R.id.thirdStar);
        fourthStar = itemView.findViewById(R.id.fourthStar);
        fifthStar = itemView.findViewById(R.id.fifthStar);

        context = itemView.getContext();
        this.onItemClickListner = onItemClickListner;

        ivChatTeacher.setOnClickListener(this);
        ivBookAppointment.setOnClickListener(this);
    }

    public void setData(User teacher){


        this.teacher = teacher;
        tvTeacherName.setText(teacher.getUsername());
        tvPrice.setText(teacher.getPrice_per_hour()+"JOD");

        if(teacher.getMax_number_per_group()==1){
            firstStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));

        }else if(teacher.getMax_number_per_group()==2){
            firstStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
            secondStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));

        }else if(teacher.getMax_number_per_group()==3){
            firstStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
            secondStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
            thirdStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));

        }else if(teacher.getMax_number_per_group()==4){
            firstStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
            secondStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
            thirdStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
            fourthStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));

        }else if(teacher.getMax_number_per_group()>4){
            firstStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
            secondStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
            thirdStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
            fourthStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
            fifthStar.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_star));
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.chatTeacherIv:
                onItemClickListner.onItemClickListener(ivChatTeacher,teacher);
                break;

            case R.id.bookTeacherIv:
                onItemClickListner.onItemClickListener(ivBookAppointment,teacher);
                break;


        }

    }
}
