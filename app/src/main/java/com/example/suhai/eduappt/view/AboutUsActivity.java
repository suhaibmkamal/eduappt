package com.example.suhai.eduappt.view;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.suhai.eduappt.R;

public class AboutUsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView ivTwitter;
    ImageView ivFacebook;
    ImageView ivPhone;
    ImageView ivEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        ivTwitter = findViewById(R.id.iv_twitter);
        ivFacebook = findViewById(R.id.iv_facebook);
        ivPhone = findViewById(R.id.iv_phone);
        ivEmail = findViewById(R.id.iv_mail);
        ivTwitter.setOnClickListener(this);
        ivFacebook.setOnClickListener(this);
        ivPhone.setOnClickListener(this);
        ivEmail.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.iv_twitter:

                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("twitter://user?user_id=1074734856628830215"));
                    startActivity(intent);
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://twitter.com/ApptEdu")));
                }
                break;

            case R.id.iv_facebook:

                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("fb://page/391770204896049"));
                    startActivity(intent);
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.facebook.com/EduAppt-391770204896049")));
                }
                break;

            case R.id.iv_mail:

                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","EduAppt@gmail.com", null));
                startActivity(Intent.createChooser(intent, "Send Email"));
                break;
            case R.id.iv_phone:

                try {
                    String phone = "+962799242617";
                    Intent intent2 = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent2);
                } catch (Exception e) {
                  e.printStackTrace();
                }
                break;
        }
    }
}
