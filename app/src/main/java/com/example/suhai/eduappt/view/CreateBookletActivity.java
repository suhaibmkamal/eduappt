package com.example.suhai.eduappt.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.presenter.CreateBookletPresenter;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;

import java.util.ArrayList;

public class CreateBookletActivity extends AppCompatActivity implements BookletsView, View.OnClickListener {

    RadioGroup rgEduType;
    EditText etSubject;
    EditText etPrice;
    EditText etPublishDate;
    EditText etStock;
    Button btnCreateBooklet;
    CreateBookletPresenter createBookletPresenter;
    SharedPrefrenceManager sharedPrefrenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_booklet);
        setTitle(R.string.create_booklet);

        sharedPrefrenceManager = SharedPrefrenceManager.getInctence(this);
        createBookletPresenter = new CreateBookletPresenter(this,sharedPrefrenceManager);


        initViews();


    }

    private void initViews() {

        rgEduType = findViewById(R.id.rgEduType);
        etSubject = findViewById(R.id.etSubject);
        etPrice = findViewById(R.id.etPrice);
        etPublishDate = findViewById(R.id.etPublishDate);
        etStock = findViewById(R.id.etStock);
        btnCreateBooklet = findViewById(R.id.btnCreateBooklet);
        btnCreateBooklet.setOnClickListener(this);


    }

    @Override
    public void onGetSuccessfully(ArrayList<Booklet> booklets) {

    }

    @Override
    public void onCreatBookletSuccessfuly(Booklet booklet) {
        Toast.makeText(this, booklet.getSubject()+" Created Successfully", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onErrorOnLogin(Throwable e) {

        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        initObject();
        createBookletPresenter.onCreateBooklet();
    }

    void initObject(){
        createBookletPresenter.setSubject(etSubject.getText().toString());
        createBookletPresenter.setPublishedDate(etPublishDate.getText().toString());
        createBookletPresenter.setPrice(Double.valueOf(etPrice.getText().toString()));
        createBookletPresenter.setStock(Integer.valueOf(etStock.getText().toString()));

        switch (rgEduType.getCheckedRadioButtonId()){
            case R.id.rbSAT:
                createBookletPresenter.setEducationSystem(User.EducationType.SAT.name());
                break;

            case R.id.rbIGCSE:
                createBookletPresenter.setEducationSystem(User.EducationType.IGCSE.name());
                break;

            case R.id.rbTwjihi:

                createBookletPresenter.setEducationSystem(User.EducationType.TAWJIHI.name());
                break;
        }


    }
}
