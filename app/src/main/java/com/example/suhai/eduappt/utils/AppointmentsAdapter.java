package com.example.suhai.eduappt.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.Appointment;
import com.example.suhai.eduappt.model.Booklet;

import java.util.ArrayList;

public class AppointmentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public AppointmentsAdapter(ArrayList<Appointment> appointments) {
        this.appointments = appointments;

    }

    ArrayList<Appointment> appointments;
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AppointmentViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_holder_appointment, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        AppointmentViewHolder appointmentViewHolder = (AppointmentViewHolder)viewHolder;
        appointmentViewHolder.setData(appointments.get(i));

    }

    @Override
    public int getItemCount() {
        return appointments.size();
    }
}
