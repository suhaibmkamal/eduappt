package com.example.suhai.eduappt.presenter;

import com.example.suhai.eduappt.model.Appointment;
import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.retrofit.IMethodsToRequest;
import com.example.suhai.eduappt.retrofit.ServerManager;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;
import com.example.suhai.eduappt.view.BookletsView;
import com.example.suhai.eduappt.view.IAppointmentView;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class AppointmentPresenter implements IAppointmentPresenter {

    IMethodsToRequest methodsToRequest;
    IAppointmentView appointmentView;
    CompositeDisposable compositeDisposable;
    SharedPrefrenceManager sharedPrefrenceManager;

    String userToken;

    public AppointmentPresenter(IAppointmentView appointmentView, SharedPrefrenceManager sharedPrefrenceManager) {
        this.appointmentView = appointmentView;
        this.sharedPrefrenceManager = sharedPrefrenceManager;
        methodsToRequest = ServerManager.getInstance();
        compositeDisposable = new CompositeDisposable();
        userToken = sharedPrefrenceManager.getToken();
    }
    @Override
    public void getAppointments() {

        compositeDisposable.add(methodsToRequest.getAppointments("JWT " + userToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableObserver<ArrayList<Appointment>>() {
                    @Override
                    public void onNext(ArrayList<Appointment> appointments) {

                        appointmentView.onGetSuccessfully(appointments);
                    }

                    @Override
                    public void onError(Throwable e) {
                        appointmentView.onErrorOnLogin(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));

    }

    public void clearCompositeDisposable(){
        compositeDisposable.clear();
    }
}
