package com.example.suhai.eduappt.view;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.presenter.BookletsPresenter;
import com.example.suhai.eduappt.presenter.TeacherPresenter;
import com.example.suhai.eduappt.utils.BookletsAdapter;
import com.example.suhai.eduappt.utils.OnItemClickListner;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;
import com.example.suhai.eduappt.utils.TeachersAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BookletsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BookletsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookletsFragment extends Fragment implements BookletsView,OnItemClickListner,View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FloatingActionButton fabCreateBooklet;

    RecyclerView rvBooklets;

    BookletsPresenter bookletsPresenter;
    SharedPrefrenceManager sharedPrefrenceManager;


    BookletsAdapter bookletsAdapter;

    User user;

    private OnFragmentInteractionListener mListener;

    public BookletsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BookletsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BookletsFragment newInstance(String param1, String param2) {
        BookletsFragment fragment = new BookletsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_booklets, container, false);
        fabCreateBooklet = view.findViewById(R.id.fabCreateBooklet);
        fabCreateBooklet.setOnClickListener(this);
        rvBooklets=view.findViewById(R.id.rvBooklets);
        sharedPrefrenceManager = SharedPrefrenceManager.getInctence(getContext());
        bookletsPresenter = new BookletsPresenter(this,sharedPrefrenceManager);
        bookletsPresenter.getBooklets();
        user = sharedPrefrenceManager.getUser();
        if(user.getType().equals(User.UserType.STUDENT.name())){
            fabCreateBooklet.setVisibility(View.GONE);
        }
       return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
           /* throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");*/
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onGetSuccessfully(ArrayList<Booklet> booklets) {

        bookletsAdapter = new BookletsAdapter(booklets,this);
        rvBooklets.setLayoutManager(new LinearLayoutManager(getContext()));
        rvBooklets.setAdapter(bookletsAdapter);

    }

    @Override
    public void onCreatBookletSuccessfuly(Booklet booklet) {

    }

    @Override
    public void onErrorOnLogin(Throwable e) {

    }

    @Override
    public void onItemClickListener(View v, Object o) {

        Booklet booklet = (Booklet)o;
        switch (v.getId()){
            case R.id.btnBuyBooklet:
                showDialog(booklet);
                break;
        }

    }

    private void showDialog(final Booklet booklet) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage("Are you Sure You want to Buy This Booklet?");
        builder1.setCancelable(true);



        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(getContext(),"Thank you For Buying "+booklet.getSubject(),Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        final AlertDialog alert11 = builder1.create();
        alert11.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                alert11.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                alert11.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        });
        alert11.show();
    }

    @Override
    public void onClick(View v) {

        Intent i = new Intent(getContext(), CreateBookletActivity.class);
        startActivity(i);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
