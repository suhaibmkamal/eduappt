package com.example.suhai.eduappt.model;

public interface IUser {


    String getUsername();
    long getId();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);

    String getDate_of_birth();

    void setDate_of_birth(String date_of_birth);

    String getAddress();

    void setAddress(String address);

    String getCard_number();

    void setCard_number(String card_number);

    String getCard_expire_date();

    void setCard_expire_date(String card_expire_date);

    boolean isGroup_lectures();

    void setGroup_lectures(boolean group_lectures);

    long getPhone();

    void setPhone(long phone);

    String getType();

    void setType(String type);

    String getGender();

    void setGender(String gender);

    int getMax_number_per_group();

    void setMax_number_per_group(int max_number_per_group);

    int getPrice_per_hour();

    void setPrice_per_hour(int price_per_hour);

    String getEducation();

    void setEducation(String education);
    void setId(long id);

    String getToken();
    void setToken(String token);
}
