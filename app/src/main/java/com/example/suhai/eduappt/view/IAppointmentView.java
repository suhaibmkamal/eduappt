package com.example.suhai.eduappt.view;

import com.example.suhai.eduappt.model.Appointment;
import com.example.suhai.eduappt.model.Booklet;
import com.example.suhai.eduappt.model.User;

import java.util.ArrayList;

public interface IAppointmentView {

    void onGetSuccessfully(ArrayList<Appointment> appointments);
    void onGetTeahcersSuccessfully(ArrayList<User> teachers);
    void onCreatAppointmentSuccessfuly(Appointment appointment);
    void onErrorOnLogin(Throwable e);
}
