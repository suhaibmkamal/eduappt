package com.example.suhai.eduappt.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.Appointment;
import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.presenter.CreateAppointmentPresenter;
import com.example.suhai.eduappt.presenter.CreateBookletPresenter;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CreateAppointmentActivity extends AppCompatActivity implements IAppointmentView,View.OnClickListener {

    EditText etSubject;
    EditText etPrice;
    EditText etDate;
    EditText etTime;
    EditText etDuration;
    Spinner sChooseTeacher;
    EditText etAddress;
    Button   btnCreateAppointment;
    CreateAppointmentPresenter createAppointmentPresenter;
    SharedPrefrenceManager sharedPrefrenceManager;
    ArrayAdapter<String> dataAdapter;

    long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_appointment);

        sharedPrefrenceManager = SharedPrefrenceManager.getInctence(this);
        createAppointmentPresenter = new CreateAppointmentPresenter(this,sharedPrefrenceManager);

        if(getIntent().hasExtra("Tutor")){
            id = getIntent().getExtras().getLong("Tutor");

        }

        initViews();

    }

    private void initViews() {



        createAppointmentPresenter.getTeachers();
        etSubject = findViewById(R.id.etSubject);
        etPrice = findViewById(R.id.etPrice);
        etDate = findViewById(R.id.etDate);
        etTime = findViewById(R.id.etTime);
        sChooseTeacher  = findViewById(R.id.spinner);
        etAddress = findViewById(R.id.etAddress);
        etDuration = findViewById(R.id.etDuration);
        btnCreateAppointment = findViewById(R.id.btnCreateAppointment);
        btnCreateAppointment.setOnClickListener(this);






    }

    @Override
    public void onGetSuccessfully(ArrayList<Appointment> appointments) {

    }

    @Override
    public void onGetTeahcersSuccessfully(ArrayList<User> teachers) {

        ArrayList<String> names = new ArrayList<>();

        for(User teacher:teachers){

            if(teacher.getId()!=id){

                names.add(teacher.getUsername());
            }
        }

        dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, names);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        sChooseTeacher.setAdapter(dataAdapter);

    }

    @Override
    public void onCreatAppointmentSuccessfuly(Appointment appointment) {
        Toast.makeText(this, appointment.getSubject()+" Created Successfully", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onErrorOnLogin(Throwable e) {

        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        initObject();
        createAppointmentPresenter.onCreateAppointment();
    }

    void initObject(){
        createAppointmentPresenter.setSubject(etSubject.getText().toString());

        createAppointmentPresenter.setTime(etDate.getText().toString()+" "+etTime.getText().toString());
        createAppointmentPresenter.setPrice(Double.valueOf(etPrice.getText().toString()));
        createAppointmentPresenter.setDuration(etDuration.getText().toString());
        createAppointmentPresenter.setAddress(etAddress.getText().toString());
        createAppointmentPresenter.setTutor(id);



    }
}
