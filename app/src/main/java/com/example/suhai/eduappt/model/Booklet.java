package com.example.suhai.eduappt.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Booklet implements IBooklet,Serializable {

    @SerializedName("tutor")
    long tutor;
    @SerializedName("type")
    String type;
    @SerializedName("subject")
    String subject;
    @SerializedName("price")
    double price;
    @SerializedName("year_published")
    String year_published;
    @SerializedName("education_system")
    String education_system;
    @SerializedName("stock")
    int stock;

    @Override
    public long getTutor() {
        return tutor;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String getPublishedDate() {
        return year_published;
    }

    @Override
    public String getEducationSystem() {
        return education_system;
    }

    @Override
    public int getStock() {
        return stock;
    }

    @Override
    public void setTutor(long tutor) {

        this.tutor = tutor;
    }

    @Override
    public void setType(String type) {

        this.type = type;
    }

    @Override
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public void setPublishedDate(String year_published) {
        this.year_published = year_published;
    }

    @Override
    public void setEducationSystem(String education_system) {
        this.education_system = education_system;
    }

    @Override
    public void setStock(int stock) {
        this.stock = stock;
    }
}
