package com.example.suhai.eduappt.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.suhai.eduappt.R;
import com.example.suhai.eduappt.model.User;
import com.example.suhai.eduappt.presenter.TeacherPresenter;
import com.example.suhai.eduappt.utils.OnItemClickListner;
import com.example.suhai.eduappt.utils.SharedPrefrenceManager;
import com.example.suhai.eduappt.utils.TeachersAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.internal.cache.DiskLruCache;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TeachersFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TeachersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TeachersFragment extends Fragment implements TeachersView,OnItemClickListner ,ValueEventListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    RecyclerView rvTeachers;

    TeacherPresenter teacherPresenter;
    SharedPrefrenceManager sharedPrefrenceManager;


    TeachersAdapter teachersAdapter;

    DatabaseReference mDatabaseRef ;
    DataSnapshot dataSnapshot;

    User user;
    public TeachersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TeachersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TeachersFragment newInstance(String param1, String param2) {
        TeachersFragment fragment = new TeachersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_teachers, container, false);
        rvTeachers=v.findViewById(R.id.rvTeacher);
        sharedPrefrenceManager = SharedPrefrenceManager.getInctence(getContext());
        teacherPresenter = new TeacherPresenter(this,sharedPrefrenceManager);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        mDatabaseRef.child("Rooms").addValueEventListener(this);
        teacherPresenter.getTeachers();
        user = sharedPrefrenceManager.getUser();
        return v ;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            /*throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");*/
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        teacherPresenter.clearCompositeDisposable();
    }

    @Override
    public void onGetSuccessfully(ArrayList<User> users) {

        teachersAdapter = new TeachersAdapter(users,this);
        rvTeachers.setLayoutManager(new LinearLayoutManager(getContext()));
        rvTeachers.setAdapter(teachersAdapter);

    }

    @Override
    public void onErrorOnLogin(Throwable e) {
        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClickListener(View v, Object o) {

        switch (v.getId()){
            case R.id.chatTeacherIv:
                User teacher = (User)o;
                Map<String,Object> roomsMap = new HashMap<String, Object>();

                if(!dataSnapshot.child(teacher.getUsername()+","+user.getUsername()).exists()) {
                    roomsMap.put(teacher.getUsername() + "," + user.getUsername(), "");
                    mDatabaseRef.child("Rooms").updateChildren(roomsMap);
                    mDatabaseRef.child("Messages").updateChildren(roomsMap);
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put(user.getUsername(), true);
                    map.put(teacher.getUsername(), true);
                    mDatabaseRef.child("Rooms").child(teacher.getUsername() + "," + user.getUsername()).updateChildren(map);
                }
                Intent intent = new Intent(getContext(),ChatRoomActivity.class);
                intent.putExtra("roomName",teacher.getUsername()+","+user.getUsername());
                intent.putExtra("user",user);
                startActivity(intent);
                break;

            case R.id.bookTeacherIv:
                User teacherToBook = (User)o;

                Intent intent2 = new Intent(getContext(),CreateAppointmentActivity.class);
                intent2.putExtra("Tutor",teacherToBook.getId());
                startActivity(intent2);
                break;
        }

    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

        this.dataSnapshot = dataSnapshot;

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
