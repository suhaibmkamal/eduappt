package com.example.suhai.eduappt.presenter;

import com.example.suhai.eduappt.model.User;

public interface ISignupPresenter {

    void onSignup();
    void updateUser();
    void setUserName(String userName);
    void setPhone(long phone);
    void setMaxnumberInGroup(int maxNumber);
    void setIsGroupAllowed(boolean isGroupAllowed);
    void setPrice(int price);
    void validateEmail(String email);
    void setAddress(String address);
    void setBirthDate(String birthDate);
    void setCardExpireDate(String birthDate);
    void setCardNumber(String cardNumber);
    void setUserType(boolean isStudent);
    void setUserGender(boolean isMale);
    void setUserEduType(String isTawjihi);
    void validatePassword(String password, String rePassword);

    User getUser();
    void setUser(User user);
}
